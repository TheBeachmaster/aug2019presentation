## Build, Test and Deploy with Bitbucket and Bitbucket Pipelines

Bitbucket Pipelines is a CI/CD service that ships with Bitbucket that enables automated building, testing, and deployment from within Bitbucket.

---

At the core Bitbucket Pipelines is `bitbucket-pipelines.yml` file. This is a configuration-as-code file that is versioned along with your code.

Each pipeline is split into _steps_ and the maximum number of steps is currently at __10__.

Each step runs commands in a separate container instance, this implies that one can use different Docker images for different steps.

A pipeline is run depending on the context. Currently there __5__ supported contexts. Only one context can be run at a time, depending on the commit.

--- 

Contexts are defined in the `pipelines` section of the `bitbucket-pipelines.yml` and must have unique names, otherwise the `default` context is run.


---
 
```yaml 
pipelines:
  pull-requests:
    '**': # For any other branch on PR... PR from develop branch
    - parallel:
        - step:
            name: Lint Markdown
            caches:
              - node
            script:
              - yarn install 
              - yarn run lint-md
```
@[1](yaml pipelines Contexts are defined here)
@[2](yaml pull-requests The build context to run ) 
@[4](yaml parallel Defines how a context is executed)
@[5-8](yaml)
@[9-11](yaml)

--- 


- `default` : All commits are run under this context unless the commit match any of the following pipeline contexts.

- `branches` : This context, if set, runs pipelines commits that match the specified branch.

  @img[branchpipelines](branchpipelines.gif) 


--- 

- `tags` / `bookmarks` : This context runs pipelines for all _commit tags_ that match a tag/bookmark pattern.

- `pull-requests` : This pipeline context runs when there's a pull request to the repo branch.

- `custom` : This a manually triggered pipeline.
 

--- 


Let's take a look at some of the pipeline definitions. Take the following sample for reference.

`options` : These are global settings that apply to the whole pipeline.

`image` : Defines the docker image to be used through-out the pipeline. One can override this per-step or per definition.

`pipelines` : defines the pipeline and the steps to run. Should always have a definition.

`default` : This will execute all defined pipeline commands except Mercurial bookmarks, tags and branch-specific pipeline definitions.


---

`step` : This is  build execution unit with the following properties:


- `image` : The Docker image to run
- `max-time` : Maximum number of minutes that a step can run
- `caches` : The cached dependencies to use
- `services` : Separate docker containers to use during build
- `artifacts` : The files to be shared between steps
- `trigger` : Defines whether a step will require human intervention or should run automatically.
--- 

`parallel` : This enables you to run multiple steps at the same time

`script` : These are the commands to execute in sequence. Large scripts should be executed from script files.

`branches` : This section defines branch-specific builds commands to your pipelines. This will always override `default` unless otherwise.

`pull-requests` : This executes build commands to be run on pull-request. 

`pipes` : New features, aims at simplify manual tasks by just simply supplying variables.
